import React, { useState, useEffect } from 'react';
import {Link} from 'react-router-dom'
import {Button, Form, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddNewProduct() {
  const [show, setShow] = useState(false);


  const [productName,setProductName]= useState("");
  const [description,setDescription]= useState("");
  const [quantity,setQuantity]= useState("");
  const [price,setPrice]= useState("");
  const [image, setImage]= useState("");
  const [isActive, setIsActive] = useState(false);

const confirmation=(e)=>{
	e.preventDefault();

	Swal.fire({
  title: 'Are you sure?',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Add Product'
}).then((result) => {
  if (result.isConfirmed) {
    addProduct();
  }
})
};


const addProduct = async()=> {
	try{

	const product = await fetch(`${process.env.REACT_APP_API_URL}/products/`, {
	    	method: "POST",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: productName,
			    description: description,
			    price: price,
			    quantity:quantity,
			    imageUrl:image
			})
	    })
	const response = await product.json();
	// console.log(response)

	if(response.duplicate){
		Swal.fire({
				title:response.duplicate,
				icon: "error",
				text:"Please use another Product name!"
				});
		return;
	}

	if(response === true){		
		Swal.fire({
			title:"Product Successfully added",
			icon: "success"
		});

		setProductName("");
		setDescription("");
		setQuantity("");
		setPrice("");
		setImage("");

	}else{
		Swal.fire({
				title:"Something Went Wrong!",
				icon: "error"
			})
	}


	} catch (error){
		console.error(error)
	}
};


useEffect(()=>{
	if(	productName !== "" && 
		description !== "" &&
		quantity !== "" &&
		image !== "" &&
		price !== "" ) {

		setIsActive(true)
	} else {
		setIsActive(false)
	}
},[productName, description, quantity, image, price])

  return (
    <>
     	<div className="rounded shadow bg-white pt-3 pb-5 mb-5 mx-md-5 px-3">
     		<h3 align="center" className="mt-3">Add New Product</h3>
     			<div  className="border  p-1 rounded img-container">
     				<img className="add-img" src={image} alt="Product Image" />
     			</div>	
     		
         	<Form className=" mx-md-5 px-md-5" onSubmit= {(e)=> confirmation(e)}>

         		<Form.Group className="mb-3" controlId="image">
         		  	<Form.Label>Image url</Form.Label>
         		  	<Form.Control
         		    type="url"
         		    placeholder="Enter url"
         		    size="sm"
         		    autoFocus
         		    value={image}
         		    placeholder="https://energyeducation.ca/wiki/images/thumb/7/74/Batterydura.jpg/1200px-Batterydura.jpg"
         		    onChange={(e)=> setImage(e.target.value)} 
         		  	/>
         		</Form.Group>
         		
	            <Form.Group className="mb-3" controlId="productName">
	              	<Form.Label>Product Name</Form.Label>
	              	<Form.Control
	                type="text"
	                placeholder="Enter Product Name"
	                size="sm"
	                  value={productName}
         		    onChange={(e)=> setProductName(e.target.value )} 
	              	/>
	            	</Form.Group>
	            	<Form.Group className="mb-3" controlId="description">
	              	<Form.Label>Product Description</Form.Label>
	              	<Form.Control 
	              	as="textarea"
	              	rows={2} 
	              	placeholder="Enter Description"
	              	value={description}
         		    onChange={(e)=> setDescription(e.target.value)} 
	              	/>
	            </Form.Group>

	           	<Form.Group className="mb-3" controlId="quantity">
	             	<Form.Label>Product Quantity</Form.Label>
	             	<Form.Control
	               	type="number"
	               	placeholder="Enter Product Quantity"
	               	size="sm"
	               	value={quantity}
         		    onChange={(e)=> setQuantity(e.target.value)} 
	             	/>
	           	</Form.Group>

           		<Form.Group className="mb-3" controlId="price">
           	  	<Form.Label>Product Price</Form.Label>
           	  	<Form.Control
           	    	type="number"
           	    	placeholder="Enter Product price"
           	    	size="sm"
           	    	value={price}
         		    onChange={(e)=> setPrice(e.target.value)} 
           	  	/>
           		</Form.Group>

           			{ isActive ?
	     		 <Button variant="success" type="submit" id="submitBtn" size="sm">
	       		 Submit
	     		 </Button>
	     		 :
	     		 <Button variant="success" type="submit" id="submitBtn" size="sm" disabled>
	       		 Submit
	     		 </Button>
	     		}
	     		  <Button variant="danger" className="ms-3" size="sm" as={Link} to={"/products"}>
	       		 Cancel
	     		 </Button>
	     	
          	</Form>
   	</div>
    </>
  );
}
