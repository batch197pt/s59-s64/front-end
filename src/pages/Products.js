import {useState, useEffect, useContext} from 'react';
import { Button, Modal, Form } from 'react-bootstrap'
import {Navigate, Link} from 'react-router-dom';
import DataTable from 'react-data-table-component';
import Swal from 'sweetalert2';

//components
import UserContext from '../UserContext';
// import EditModal from '../components/EditModal';


export default function Products(){
 
const { user } = useContext(UserContext);
// const navigate = useNavigate();


  //modal state
const [show, setShow] = useState(false);
const handleShow = () => setShow(true);

const[search, setSearch]= useState("");
const [filteredItems,setFilteredItems]= useState([]);
  
  
const [products, setProducts] = useState([]);

//modal data
const [id, setId]= useState("");
const [productName,setProductName]= useState("");
const [description,setDescription]= useState("");
const [price, setPrice] = useState("");
const [quantity, setQuantity] = useState("");
const [image, setImage] = useState("");

const getData= async() => {
  try{
    const product = await fetch(`${process.env.REACT_APP_API_URL}/products/`,{
        method: "GET",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      }
    });
    // console.log(product)
    const productData = await product.json();
    setProducts(productData);
    setFilteredItems(productData);
    // console.log(productData)
    // console.log(productData)
  } catch (error) {
    console.log(error);
  }
};

const columns = [
  {
    name: "Image",
    selector: (row) => <img src={row.imageUrl} className="product-image" alt="Product img"  />
  },
  {
    name: "Name",
    selector: (row) => row.productName,
    // grow:1,
    // wrap: true

  },
  {
    name: "Description",
    selector: (row) => row.description,
    grow:3,
    wrap: true
  },
  {
    name: "Quantity",
    selector: (row) => row.quantity,
    width: "100px"
  },
  {
    name: "Price",
    selector: (row) => row.price.toFixed(2)
  },
  {
    name: "Status",
    selector: (row) => row.isActive ? 'Unarchive' : 'Archive',
     width:"100px"
  },
  {
    name:"Action",
    width:"160px",
    cell:(row)=> {
      return ( 
      <div className="d-flex flex-wrap " > 
        <button 
        className="btn btn-sm btn-warning px-1 me-1 mt-1" 
         style={{width:'35px'}}
        onClick={()=> editModal(row._id)} > Edit</button>

        {
         row.isActive ? 
         <button
        className="btn btn-sm btn-danger px-1 mt-1" 
        onClick={()=> confirmArchive(row._id)} > Archive</button>
        :
         <button
        className="btn btn-sm btn-secondary px-1 mt-1" 
        style={{width:'80px'}}
        onClick={()=> confirmaUnarchive(row._id)} > UnArchive</button>
      }
      </div>
      )
    }

  }
];

const confirmArchive=(id)=>{

  Swal.fire({
  title: 'Are you sure?',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Archive Product'
}).then((result) => {
  if (result.isConfirmed) {
    archive(id)
  }
})
};

const confirmaUnarchive=(id)=>{
  Swal.fire({
  title: 'Are you sure?',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Unarchive Product'
}).then((result) => {
  if (result.isConfirmed) {
    unArchive(id)
  }
})
};

//archive product
const archive=(id)=>{
 fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`,{
        method: "PATCH",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
      if(data ===true){
          Swal.fire({
          title:"Product has been Archived!",
          icon: "success"
        });
          getData();
      }else{
        Swal.fire({
        title:"Something Went Wrong!",
        icon: "error"
         });
      }
    })
    .catch(error => {
        console.log(error)
    })
};

  //unarchive product
  const unArchive=(id)=>{

  fetch(`${process.env.REACT_APP_API_URL}/products/${id}/unarchive`,{
        method: "PATCH",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        if(data ===true){
            Swal.fire({
            title:"Product has been Unarchived!",
            icon: "success"
          });
            getData();
        }else{
          Swal.fire({
          title:"Something Went Wrong!",
          icon: "error"
           });

        }
      })
      .catch(error => {
        console.log(error)
    })

    };


//get all product data
useEffect(()=> {
  getData();

},[]);


// filter/search products
useEffect(()=>{
  const result = products.filter(product => {
    let status = ""
    if(product.isActive === true){
      status = "Active"
    }else{
    status = "Not Active"
    }
      return product.description.match(search) || product.productName.match(search) || product.price.toString().match(search) || product.quantity.toString().match(search) ||status.match(search) ;

  });
 setFilteredItems(result);
},[search])


//close modal
const handleClose = () => {
      setId("");
      setProductName("");
      setDescription("");
      setPrice("");
      setQuantity(""); 
      setImage("");
  setShow(false)
};


//modal data
const editModal=(productId) => {
  handleShow(); 

  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
  .then(res => res.json())
  .then(data => { 
    setId(data._id);
    setProductName(data.productName);
    setDescription(data.description);
    setPrice(data.price);
    setQuantity(data.quantity);
    setImage(data.imageUrl);
    }); 
};

const confirmEdit=(e)=>{
   e.preventDefault();
  Swal.fire({
  title: 'Are you sure?',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Edit Product'
}).then((result) => {
  if (result.isConfirmed) {
  editProduct();
  }
})
};

//edit product by id
const editProduct = (e) => {
 
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
        method: "PUT",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          productName: productName,
          description: description,
          price: price,
          quantity:quantity,
          imageUrl:image
      })
    })
    .then(res=> res.json())
    .then(data => {
      
      if(data === true ){
        handleClose();
        Swal.fire({
          title:"Edit Success",
          icon: "success"
       });

       getData();

      }else{
         Swal.fire({
           title:"Something Went Wrong",
           icon: "Error"
        });
      }
  })
};


  return (
    (user.id !== null && user.isAdmin===true) ?
    <>
      <h4 className="text-secondary">Products List</h4>
    <Button className="mt-2 mb-3" as={Link} to="/products/addProduct" size="sm" >Add New Product</Button>
     <div className="border shadow"> 
        <DataTable 
      className="" 
      columns={columns} 
      data={filteredItems}
      pagination 
      fixedHeader
      highlightOnHover
      subHeader
      responsive
      subHeaderComponent = {
        <input 
          type ="text"
          className = " mt-4 form-control"
          size="sm"
          style={{width:"200px"}}
          placeholder ="Search . . ."
          value={search}
          onChange={e => setSearch(e.target.value) }
          />
      }
      />
     </div> 
    

        <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Modal title</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div  className="border  p-1 rounded d-flex justify-content-center">
            <img height="80"  src={image} alt="Product" />
          </div>
             <Form  onSubmit={(e) => confirmEdit(e)}>

            <Form.Group className="mb-3" controlId="image">
                <Form.Label>Image url</Form.Label>
                <Form.Control
                type="url"
                placeholder="Enter url"
                size="sm"
                value={image}
                onChange={(e)=> setImage(e.target.value)} 
                />
            </Form.Group>
            
              <Form.Group className="mb-3" controlId="productName">
                  <Form.Label>Product Name</Form.Label>
                  <Form.Control
                  type="text"
                  size="sm"
                    value={productName}
                onChange={(e)=> setProductName(e.target.value )} 
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="description">
                  <Form.Label>Product Description</Form.Label>
                  <Form.Control 
                  size="sm"
                  as="textarea"
                  rows={2} 
                  value={description}
                onChange={(e)=> setDescription(e.target.value)} 
                  />
              </Form.Group>

              <Form.Group className="mb-3" controlId="quantity">
                <Form.Label>Product Quantity</Form.Label>
                <Form.Control
                  type="number"
                  size="sm"
                  value={quantity}
                onChange={(e)=> setQuantity(e.target.value)} 
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="price">
                <Form.Label>Product Price</Form.Label>
                <Form.Control
                  type="number"
                  size="sm"
                  value={price}
                onChange={(e)=> setPrice(e.target.value)} 
                />
              </Form.Group>
        <div className="d-flex justify-content-end">
         <Button variant="danger" className="me-3" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" type="submit" id="submitBtn"> Edit </Button>

        </div>
       
            </Form>
        </Modal.Body>
    
      </Modal>

     </>
     : <Navigate to="/" />
  );
}



 