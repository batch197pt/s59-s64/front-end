
import	{ useState , useEffect, useContext} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import OrderAccordion from '../components/OrderAccordion'; 


import Pagination from '../components/Pagination';
import Posts from '../components/Post';

export default function AllOrders(){
    const {user} = useContext(UserContext);
 
	const[data, setData] = useState([]);

	const[posts, setPosts] = useState([]);
	const [loading,setLoading]= useState(false);
	const [currentPage,setCurrentPage]= useState(1);
	const [postsPerPage,setPerPage] = useState(10);

useEffect(() =>{
	const fetchPost = async() =>{
		setLoading(true);
		const res = await fetch(`${process.env.REACT_APP_API_URL}/orders`,{
            method: "GET",
            headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
		const resData = await res.json();
		setPosts(resData);
		setLoading(false);
	}
	fetchPost();
	
},[]);


	const set = new Set();
	for( let i = 0 ; i < posts.length; i++){
		set.add(posts[i].userId)
	}
	console.log(set)
	set.forEach(e=>{
			for(let i = 0; i< posts.length; i++ ){

			}
		console.log(e)
	})
useEffect(() =>{
	
	// console.log(currentPosts)
},[posts]);
    
 
   const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost,indexOfLastPost); 

	return (
            
                (user.id !== null && user.isAdmin) ? 
                <>
                <h3 className="text-secondary">All Orders</h3>          
                <div className="container overflow-auto">
            	<Posts  posts={currentPosts} loading={loading} />
            	<Pagination postPerPage={postsPerPage} totalPosts={posts.length} paginate={paginate}/>
                     {/*{orderDetails}*/}
          
                </div>                   
                  </>          	   
            : (user.id !== null && !user.isAdmin) ? 
            <> <Navigate to="/dashboard" /> </>
            : <Navigate to="/" />
            
            
  );
		
}