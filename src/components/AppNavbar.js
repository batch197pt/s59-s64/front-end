
import { useContext} from 'react';
import {Navbar, Nav, Container } from 'react-bootstrap';
import { Link , useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function AppNavbar(){
const navigate = useNavigate();
	const logout = () => {
		  Swal.fire({
          title: 'Logout?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.isConfirmed) {
              navigate("/logout")
          }else{
            
          }
        })

	}

	const { user } = useContext(UserContext);
	
	return (
	<Navbar  sticky="top" variant="dark" expand="md">
	    <Container fluid>
	    {
	    	(user.id !== null && user.isAdmin === true) ?
	    	 <Navbar.Brand  as={Link} to="/dashboard">e-commerce-app-guillermo</Navbar.Brand>
	    	 :
	    	  <Navbar.Brand as={Link} to="/">e-commerce-app-guillermo</Navbar.Brand>
	    }
		   
		    <Navbar.Toggle aria-controls="navbarScroll" />
		    <Navbar.Collapse id="navbarScroll" className="">
		        <Nav
		         className="me-auto w-100 justify-content-end">
			      {
			       		(user.id !== null && user.isAdmin === true) ?
			       		<>
			       			<Nav.Link as={Link} to="/dashboard">Dashboard</Nav.Link>
			       			<Nav.Link as={Link} to="/products">Products</Nav.Link>
			       			<Nav.Link as={Link} to="/users">Users</Nav.Link>
			       			<Nav.Link as={Link} to="/allOrder">Orders</Nav.Link>
			       			<Nav.Link onClick={()=> logout()} >Log out</Nav.Link>
			       		</>
			       			: (user.id !== null && user.isAdmin === false)?
			       		<>
			       			<Nav.Link as={Link} to="/">Home</Nav.Link>
			       			<Nav.Link as={Link} to="/viewProducts">Products</Nav.Link>
			       			<Nav.Link as={Link} to="/order">Orders</Nav.Link>
			       			<Nav.Link  as={Link} to="/cart">Cart</Nav.Link>
			       			<Nav.Link as={Link} to="/account">Account</Nav.Link>
			       			<Nav.Link onClick={()=> logout()} >Log out</Nav.Link>
			       		</>
			       			:
			       			<>
			       			 <Nav.Link as={Link} to="/">Home</Nav.Link>
			       			 <Nav.Link as={Link} to="/viewProducts">Products</Nav.Link>
			       			 <Nav.Link as={Link} to="/login" >Login</Nav.Link>
			       			 <Nav.Link as={Link} to="/register">Register</Nav.Link>
			      			</>
			       }
			       
		        </Nav>
	     </Navbar.Collapse>
	    </Container>
	 </Navbar>

	)
}