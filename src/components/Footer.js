import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
 import { faPhone} from '@fortawesome/free-solid-svg-icons';
 // import { faFacebook} from '@fortawesome/free-regular-svg-icons';
 import{faFacebookF, faGoogle} from '@fortawesome/free-brands-svg-icons';

export default function Footer(){
	return (
		 <footer className="py-4 w-100">
		 <h2 align="center">Contact Us!</h2>
        <div className=" d-flex flex-wrap justify-content-center">
            <p className="mx-2 my-1">
                <a className="text-white" href="https://www.facebook.com/gabriel.guillermo2/" target="_blank">
                <FontAwesomeIcon icon={faFacebookF} />
                    <small> gabriel.guillermo2</small>
                </a>
            </p>
            <p className="text-white mx-2 my-1 ">
                 <FontAwesomeIcon icon={faPhone} /> <small> 09758844512 (globe)</small>
            </p>
            <p className=" text-white mx-2 my-1">
                <FontAwesomeIcon icon={faGoogle} /> <small> gabrieldguillermo@gmail.com</small>
            </p>
        </div>
        <p className="text-center my-auto text-white"><small>All Rights Reserve 2022</small></p>
    </footer>
		)

}