
import { Col , Card } from 'react-bootstrap'; 
import { Link } from 'react-router-dom';

export default function ProductsCards({props}){
	const {_id,productName,price,quantity,imageUrl, description} = props;
	
	function productDetail(){

	}


	return(
		<Col className="col-6 col-sm-6 col-md-4 col-lg-3   p-2" onClick={()=> productDetail()} as={Link} to={`/product/${_id}`} >
		<Card className="shadow" style={{height:"350px"}}>
	 <div className="p-3" >
			 <Card.Img variant="top" src={imageUrl}   />
		</div>
     
      <Card.Body  className="text-secondary ">
        <Card.Subtitle className="product-name">{productName}</Card.Subtitle>
        <Card.Text className="product-description">
       {description}
        </Card.Text>
        <Card.Text className="mb-2"> {(price).toLocaleString('en-US', {
  style: 'currency',
  currency: 'PHP',
})}
        </Card.Text>
         <Card.Text className="mb-2 product-quantity">Product Available:  {quantity} 
        </Card.Text>
      </Card.Body>
    </Card>
    </Col>
    )
}