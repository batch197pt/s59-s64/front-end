
export default function Pagination({postPerPage,totalPosts,paginate}){
	const pageNumbers = [];

	for(let i = 1; i <= Math.ceil(totalPosts / postPerPage); i++){
		pageNumbers.push(i)
	}
	return(
		<nav>
			<ul className="pagination bg-light">
				{
					pageNumbers.map(number =>{
					return (
							<li key={number} className="page-item">
								<a 
								onClick={()=>paginate(number)}
								href="#"  
								className="page-link">
								{number}
								</a>
						</li>
						)
				})
				}

			</ul>
		</nav>

		)
}